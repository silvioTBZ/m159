# Umfrage Directorys in Lehrbetrieben - Modul 159

<hr>

## Lektionen Plan

| Schuljahr | Links Umfragen                                               | Links Antworten                                              |
| --------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 2022/23   |                                                              | [Antworten 2022/23](https://forms.office.com/Pages/DesignPageV2.aspx?subpage=design&FormId=xf0z91USjU23kyvHrKDyFICfen3C3e5ItA7GqwSxedFUOUE4TE0xVDhFVjIzUlhLRUxUVVlNTko2TC4u&Token=f670858d71ae411eae24260e2018d57c) |
| 2023/24   | [Umfrage 2032/24](https://forms.office.com/Pages/ResponsePage.aspx?id=xf0z91USjU23kyvHrKDyFICfen3C3e5ItA7GqwSxedFUQVRaOTFNODQ3UUpSQkJXM1lQVVNCMTUzTy4u) | [Antworten 2023/24](https://forms.office.com/Pages/DesignPageV2.aspx?subpage=design&FormId=xf0z91USjU23kyvHrKDyFICfen3C3e5ItA7GqwSxedFUQVRaOTFNODQ3UUpSQkJXM1lQVVNCMTUzTy4u) |
| 2024/25   |                                                              |                                                              |
| 2025/26   |                                                              |                                                              |
| 2026/27   |                                                              |                                                              |
| 2027/28   |                                                              |                                                              |
|           |                                                              |                                                              |
|           |                                                              |                                                              |
|           |                                                              |                                                              |
|           |                                                              |                                                              |
