# Proxmox

```
***Hinweis:***

Proxmox ist die Umgebung der TBZ für alle Schüler die keine eigene Umgebung im Geschäft, Zuhause oder bei einem Anbieter beziehen bzw. aufsfetzen können. 

```

https://tbz.hopto.org:8006

Proxmox Login: Erhaltet Ihr im Teams Kanal

## Setup

Lernende welche die TBZ Proxmox-Umgebung  nutzen erhalten 2 Server und 2 Client VM's 

- 2x Windows Server 2022 Standard
- 2x Windows 10 Client VM
- 1x PF Sense 2.6 Router VM
- Jeder User ist in einem separaten Pool (Pool ID z.b. Pool5)
- Vergeben Sie sämtlichen Netzwerkschnittstellen, welche in Ihrem definierten Subnetz sind die VLAN-ID entsprechend Ihres Pooles (z.B. Pool5 = 5)
- Die WAN-Schnittstelle bei der Router-VM soll auf DHCP und ohne VLAN-ID auf dem Netzwerk Interface konfiguriert sein

Warum habe ich nur zwei Server, wenn ich für die Aufgaben mehrere Server benötige? Leider können wir im Moment keine zusätzlichen Server-VMs bereitstellen, da wir auch die verfügbaren Ressourcen berücksichtigen müssen. Dies sollte jedoch kein Problem darstellen, da Sie nach Abgabe und Bestätigung der Aufgabe durch die Lehrperson den Server neu aufsetzen können, und er somit für die erledigte Aufgabe nicht mehr benötigt wird.



### Video mit Instruktionen: 

https://vimeo.com/834389792/9e43089766

### Snapshots

Ihr könnt Snapshots erstellen und so Zwischenstände speichern. Machen Sie Snapshots am besten wenn das System heruntergefahren ist, damit das RAM nicht gespeichert werden muss (= weniger Probleme).

**Info:** Falls Sie keine Snapshots machen können entfernen Sie das TMP-Modul unter Hardware. 



### VM-Einstellungen

[VM-Benennen](https://gitlab.com/ch-tbz-it/Stud/m158/-/blob/main/04_Unterrichtsressourcen/02_Bilder/VM-benennen.png)



### SCSI-Treiber laden

Wenn bei der Installation des Betriebssystems keine Harddisk angezeigt wird, dass liegt dies daran, dass die Treiber für die Installation des Betriebssystems manuell geladen werden müssen. 

Ihre vorkonfigurierte VM sollte ein virtuelles DVD-Rom Laufwerk mit der entsprechenden ISO-Datei verbunden haben.

<img src="https://gitlab.com/ch-tbz-it/Stud/m159/-/raw/main/02_Unterrichtsressourcen/01_Bilder/SCSI.png?ref_type=heads" width="640" height="480">



### Probleme mit IP-Adresse setzen (APIPA)

[Schauen Sie hier unter Punkt 3](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/03_Fachliteratur&Tutorials/Tools/VM-Ratgeber.pdf?ref_type=heads)



### SPICE 

Zur Alternative zu "No VNC" Verbindung kann SPICE als Verbidnungstyp verwendet werden. (Before you switch the Display to SPICE install the Windows SPICE guest tools 0.132 or higher)

- Ihre vorkonfigurierte VM sollte ein virtuelles DVD-Rom Laufwerk mit den SPICE Guest Tools bereits verbunden haben.
  Alternativ download über: https://www.spice-space.org/download.html
- Virt-Manager muss auf dem Gerät installiert werden, von welchem aus Sie auf die VM zugreifen möchten
  - VIRT Manager download: https://virt-manager.org/download



Um mit SPICE zu verbinden, können Sie bei der VM -> Console und dann SPICE auswählen. 

"ALT + CTRL + R" um den Curser innerhalb von SPICE  wieder freizugeben.

