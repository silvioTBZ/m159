**Instruktionen zum Modul 159**

# Einzelarbeit

Im Modul 159 arbeitet jeder Schüler in seiner eigenen Umgebung. Während
des Moduls ist ein Austausch zwischen den Lernenden erwünscht. In der
Vergangenheit hat sich gezeigt, dass Schüler, die oft besser und
schneller vorankommen als andere, diesen beim Lösen von Aufgaben
geholfen haben und dabei noch mehr profitiert haben.

# Austauschpartner/Austauschgruppe

Suchen Sie sich eine Austauschs-Partnerin oder Partner oder eine
Austauschgruppe. Diese Schüler sind Ihre ersten Anlaufstellen, wenn Sie
Fragen zu den Aufgaben während des Moduls haben.

Sollten Ihre Partner keine Antwort für Sie haben, können Sie natürlich
jederzeit die Lehrperson kontaktieren.

# Virtuelle Umgebung

Für eine erfolgreiche Durchführung des Moduls 159 \'Directory Services
konfigurieren und in Betrieb nehmen, benötigen Sie eine virtuelle
Umgebung mit Internetanschluss. Je nachdem, welche Konfiguration Sie
wählen, benötigen Sie zwischen zwei und 6 virtuellen Maschinen.

Absolute Minimalkonfiguration

-   1x Windows Server 2019 Standard-Desktop (oder neuer)

-   1x Windows 10 Pro/Edu (oder neuer)

[Systemanfordeurngen](https://learn.microsoft.com/en-us/windows-server/get-started/hardware-requirements)

### Die drei Umgebungen visualisiert

[Basic](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/01_Bilder/%C3%9Cberblick%20Umgebung%20m159%20-%20Basic.png)

[Advanced](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/01_Bilder/%C3%9Cberblick%20Umgebung%20m159%20-%20Advanced.png)

[Expert](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/01_Bilder/%C3%9Cberblick%20Umgebung%20m159%20-%20Expert.png)

Hinweis: Es ist auch möglich mit Basic die Note 6 zu erreichen. Es geht bei dieser Abstufung mehr um die Ressourcen Ihre Umgebung, als um den Schwierigkeitsgrad.

### Standorte

Wenn Sie die Standorte ebenfalls abbilden möchten, benötigen Sie einen
virtuellen Router (z.B. pfSense) mit lediglich 128MB Ram und 30MB
Harddisk.

**Hinweise zur Umgebung:**

Windows-Domänencontroller müssen im Dauerbetrieb laufen und sollten so
selten wie möglich neu gestartet werden. Wenn es dennoch nötig ist,
sollte eine bestimmte Reihenfolge eingehalten werden. Mehr dazu erfahren
Sie später.

Frühere Durchführungen des Moduls mit den VMs auf den Schülerlaptops
haben gezeigt, dass es \'nur mit Fehlern\' möglich ist, eine AD-Umgebung
aufzubauen.

Jeder Schüler soll deshalb idealerweise im Geschäft oder zuhause einen
Server/PC einrichten, auf dem während des Moduls eine Windows Server
Umgebung betrieben werden kann.

**Achtung:** Parallels Home & Students kann nicht verwendet werden, da
man nicht zwei Network-Interfaces einrichten kann pro VM.

# Backup

Für das Backup Ihrer virtuellen Maschinen sind Sie jederzeit selbst
verantwortlich. Stellen Sie sicher, dass Sie nach jedem Schultag eine
Sicherungskopie Ihrer Umgebung erstellen können. Möglicherweise sollten
Sie eine Virtualisierungsumgebung installieren, die Snapshots
unterstützt, um sicherzustellen, dass keine Arbeit verloren geht.

# Unterricht & selbstständiges Arbeiten

Der Unterricht wird in der Regel in einen theoretischen und einen
praktischen Teil unterteilt. In den ersten zwei Lektionen wird die
Lehrperson Inputs zu einem spezifischen Thema im Zusammenhang mit dem
Directory Service geben, gefolgt von Ihrer eigenen Arbeit an Ihrer
Umgebung.

**Achtung:** Falls Sie nicht in der Lage sind, an Ihren VMs zu arbeiten,
weil Sie beispielsweise vergessen haben, den Server zuhause oder im
Geschäft zu starten, müssen Sie den Unterricht verlassen. Es liegt in
Ihrer Verantwortung sicherzustellen, dass die VMs während des
Unterrichts verfügbar sind!

# Aufgaben

Das Aufsetzen Ihrer AD-Umgebung ist in 24 Aufgaben eingeteilt. Sie
finden diese Aufgaben im Repository im Ordner 03_Aufgaben.

## Aufgaben abgeben

Um eine Aufgabe abzugeben, erstellt ihr bitte einen Video-Screencast mit
eigenen Kommentaren. Es ist möglich, mehrere Aufgaben in einem einzigen
Screencast einzureichen. Sollte der Screencast nicht die volle Punktzahl
erreichen, könnt ihr den fehlenden Inhalt mit einem Printscreen
ergänzen. Es ist jedoch nicht erlaubt, ausschließlich Printscreens als
Abgabemethode zu verwenden.

Im Screencast-Video muss klar erkennbar sein, dass es sich um eure
eigene Arbeitsumgebung handelt und nicht um die eines Dritten. Ihr könnt
dies nachweisen, indem ihr zu Beginn jedes Videos individuelle
Einstellungen aufruft, wie beispielsweise den Domainnamen oder
Ähnliches.

Es kann sein, dass in den Aufgaben noch eine Abgabe mit Printscreen
erwähnt ist. In diesem Fall ist nicht ein Printscreen verlangt, sondern
derselbe Inhalt soll in Ihrem Screencast gezeigt werden.

Generell könnt ihr Aufgaben bis zum letzten Schultag (Tag 10)
einreichen. Bitte beachtet jedoch **dringend**, dass die Lehrperson am
letzten Schultag nicht gleichzeitig für alle verfügbar sein wird. Die
Abwicklung erfolgt nach dem Prinzip \'Wer zuerst kommt, mahlt zuerst\'.

Wichtig: Wenn ihr Aufgaben abgeben möchtet, markiert bitte die
Lehrperson in eurem privaten Kanal in MS Teams! Chatnachrichten werden
von der Lehrperson nicht beantwortet.

Folgende Kriterien gelten fürs Abgeben von Aufgaben

-   Eine Kontrolle verifiziert, dass Sie eine Aufgabe selbstständig
    erledigt, haben

-   Den Weg zum Resultat einer Aufgabe müssen Sie **nicht** Schritt für
    Schritt dokumentieren

-   Eine Aufgabe kann nur 2x abgegeben werden (1x Screencast und
    anschliessend 1x Printscreen)

# Portfolio

In diesem Dokument wird am Anfang die ganze «Active Directory» Umgebung
geplant.

Sie erhalten die Vorlage «M159-Portfolio-Vorlage.docx» für die Planung
Ihrer Umgebung.

Das Portfolio kann ausgedruckt und zu Beginn von Hand ausgefüllt werden,
wenn Sie dies möchten. Sie müssen die Daten aber in einem zweiten
Schritt digital erfassen. Für einen Ausdruck fragen Sie die Lehrperson.

# Arbeitsumgebung

Jeder Schüler erhält im MS Team einen privaten Kanal. In diesem Kanal
sind unter Dateien folgende Inhalte.

-   Link Ihren Screencasts

-   Portfolio oder Link zum Portfolio

-   Bewertungsfile (leserechte)

# Modulnote 

Die Modulnote wird anhand der bekannten Formel resultierend aus Ihren
erreichten Punktezahl anhand der abgegebenen Aufgaben eruiert.

((Erreichte Punkte / Maximale Punkte) \* 5)) + 1 = Modulnote
